import configparser

class Metadata:
    """
    Interface to the metadata for a given spectrum.
    """
    def __init__(self, filename):
        self.config = configparser.ConfigParser()
        self.config.read(filename)

    @property
    def laser(self):
        return(self.config.get("microscope", "laser"))

    @property
    def filter(self):
        return(self.config.get("microscope", "filter"))

    @property
    def hole(self):
        return(self.config.get("microscope", "hole"))

    @property
    def objective(self):
        return(self.config.get("microscope", "objective"))
    
    @property
    def sample(self):
        return(self.config.get("microscope", "sample"))
    
    @property
    def integration_time(self):
        return(float(self.config.get("acquisition", "integration")))

    @property
    def repetitions(self):
        return(int(self.config.get("acquisition", "repeat")))

