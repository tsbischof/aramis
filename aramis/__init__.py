import csv

import spectrum_analysis as sa

from .Metadata import Metadata

## These values were measured by Emory Chan, 2015-04-25
## ND 	power (mW) 	Transmission
##0 	66.92 	
##0.3 	22.65 	0.338463837
##0.6 	14.25 	0.212940825
##1 	8.53 	0.127465631
##2 	3.45 	0.051554094
##3 	0.297 	0.004438135
filter_transmission = {
    "1064": {
        "---": 1,
        "D 0.3": 0.338463837,
        "D 0.6": 0.212940825,
        "D 1": 0.127465631,
        "D 2": 0.051554094,
        "D 3": 0.004438135}}

def exposure_time(integration_time):
    """
    Get the true exposure time (laser on time) from the set integration time.

    Based on 
    """
    return(0.97*integration_time + 0.029)

##from .Acquisition import Acquisition

def get_spectrum(filename):
    wavelengths = list()
    counts = list()
    with open(filename) as stream_in:
        for wavelength, count in csv.reader(stream_in, delimiter="\t"):
            wavelengths.append(float(wavelength))
            counts.append(int(count))

    return(sa.Spectrum(wavelengths, counts))

def get_spectra(filename):
    with open(filename) as stream_in:
        reader = csv.reader(stream_in, delimiter="\t")
        line = next(reader)
        n_axes = len(list(filter(lambda x: x == "", line)))
        
        wavelengths = list(map(float, line[n_axes:]))

        for line in reader:
            t = float(line[0])
            axis = list(map(float, line[:n_axes]))
            counts = list(map(int, line[n_axes:]))
            yield(axis, sa.Spectrum(wavelengths, counts))

def get_metadata(filename):
    return(Metadata(filename))
