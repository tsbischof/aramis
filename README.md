# aramis
Tools for analysing ascii-format output of the Jobin Yvon Aramis microscope.

## Installation
This code is tested against Python 3.4 but should be compatible with 2.x. There are no external dependencies.

## Usage
To access measurement metadata, the code expects an INI-style file with the values:
	import aramis

	metadata = aramis.get_metadata("experiment")
	print(metadata.integration_time)

Viewing a single spectrum:

	spectrum = aramis.get_spectrum("experiment")

Iterating through many spectra:

	for position, spectrum in aramis.get_spectra("experiment")
		...

## Issues
There is an issue tracker hosted through Bitbucket.
